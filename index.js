const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const {check, validationResult} = require('express-validator');
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/mywebsite', {
    useNewUrlParser: true
});

const fileUpload = require('express-fileupload');
const session = require('express-session');

const Contact = mongoose.model('Contact',{
    title:String,
    slug:String,
    message: String,
    myimage: String,
    menuname: String
} );
const Admin = mongoose.model('Admin', {
    username: String,
    password: String
});

const Logo = mongoose.model('Logo',{
    headertext:String,
    footertext: String
});

var myApp = express();
myApp.use(session({
    secret: 'superrandomsecret',
    resave: false,
    saveUninitialized: true
}));
myApp.use(fileUpload());
myApp.use(bodyParser.urlencoded({ extended:false}));

myApp.use(bodyParser.json());

myApp.set('views', path.join(__dirname, 'views'));
myApp.use(express.static(__dirname+'/public'));
myApp.set('view engine', 'ejs');


//---------------- Routes ------------------//
//

myApp.get('/', function(req, res){
    // below line will get the content from contacts table using slug:'home'
    Contact.findOne({slug: 'home'}).exec(function(err, contacts){
        // below line will get the all content from contacts table.
        Contact.find({}).exec(function(err, navLinks){
             // below line will get the header and footer text from logos table using id.
            Logo.findOne({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layout){
               // here am telling to render index and sending the following
               // page : this will hold the home page content
               // navLinks: this has the data from contacts page which i need to display navigation
                // layout : this has the data for website header text and footer text.
               res.render('index', {page:contacts, navLinks: navLinks, layout: layout});
            });
        });
    });
});


myApp.get('/add',function(req, res){

    if(req.session.userLoggedIn){
        Logo.find({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layout){
            res.render('add', {layout: layout[0]});
        });
    }
    else{
        Contact.find({}).exec(function(err, navLinks){
            Logo.find({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layout){
                res.render('login', {navLinks: navLinks, layout: layout[0]});
            });

        });
    }
    
});
myApp.post('/add',[
    check('title', 'Please enter the title').not().isEmpty(),
    check('slug', 'Please enter a slug').not().isEmpty(),
],function(req, res){
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        var errorsData = {
            errors: errors.array()
        };
        res.render('add',errorsData);
    }
    else{
        var imageName = req.files.myimage.name;
        var image = req.files.myimage;
        var imagePath = 'public/contact_images/'+imageName;
        image.mv(imagePath,function(err){
            console.log(err);
        });
        var title = req.body.title;
        var slug = req.body.slug;
        var message = req.body.message;
        var menuname = req.body.menuname;
        var myContact = new Contact({
            title: title,
            slug: slug,
            message: message,
            myimage: imageName,
            menuname: menuname
        });
        myContact.save().then( ()=>{
            console.log('New contact created');
        });
        var pageData = {
            title: title,
            slug: slug,
            message: message,
            menuname: menuname
        };
        res.render('contactthanks',pageData);
    }
});

// ------------ New Routes ---------------------

myApp.get('/login',function(req, res){
    Contact.find({}).exec(function(err, navLinks){
        Logo.find({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layout){
            res.render('login', {navLinks: navLinks, layout: layout[0]});
        });
    });
});

myApp.post('/login',function(req, res){
    var username = req.body.username;
    var password = req.body.password;

    Admin.findOne({username:username, password: password}).exec(function(err, admin){
        req.session.username = admin.username;
        req.session.userLoggedIn = true;
        res.redirect('/view-pages');
    });
});

myApp.get('/logout',function(req, res){
    Contact.find({}).exec(function(err, navLinks){
        Logo.find({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layout){
            res.render('logout', {navLinks: navLinks, layout: layout[0]});
        });
    });
});

myApp.get('/view-pages',function(req, res){
    if(req.session.userLoggedIn){
        Contact.find({}).exec(function(err, contacts){
            Logo.find({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layout){
                res.render('view-pages', {contacts:contacts, layout: layout[0]});
            });
        });
    }
    else{
        Contact.find({}).exec(function(err, navLinks){
            Logo.find({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layout){
                res.render('login', {navLinks: navLinks, layout: layout[0]});
            });
        });
    }
});

myApp.get('/edit/:id',function(req, res){
    var id = req.params.id;
    if(req.session.userLoggedIn){
        Contact.findOne({_id:id}).exec(function(err, contact){
            Logo.find({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layout){
                res.render('edit', {contact:contact, layout: layout[0]})
            });
        });
    }
    else{
        Contact.find({}).exec(function(err, navLinks){
            Logo.find({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layout){
                res.render('login', {navLinks: navLinks, layout: layout[0]});
            });
        });
    }
});

myApp.post('/edit/:id',function(req, res){
    //fetch all the data from URL and form
    var id = req.params.id;
    var imageName = req.files.myimage.name;
    var image = req.files.myimage;
    var imagePath = 'public/contact_images/'+imageName;
    image.mv(imagePath,function(err){
        console.log(err);
    });
    var title = req.body.title;
    var slug = req.body.slug;
    var message = req.body.message;
    var menuname = req.body.menuname;
    //fetch the contact with the id from URL from the database
    Contact.findOne({_id:id}).exec(function(err, contact){
        //edit the fetched object from the database
        contact.title = title;
        contact.slug = slug;
        contact.message = message;
        contact.menuname = menuname;
        contact.myimage = imageName;
        //save the updated contact into the database
        contact.save().then( ()=>{
            console.log('Contact updated');
        });       
    });
    //redirect to all contacts page
    res.redirect('/view-pages');
});

myApp.get('/delete/:id',function(req, res){
    var id = req.params.id;

    if(req.session.userLoggedIn){
        Contact.findByIdAndDelete({_id:id}).exec(function(err, contact){
            Logo.find({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layout){
                res.render('delete', {layout: layout[0]});
            });
        });  
    }
    else{
        Contact.find({}).exec(function(err, navLinks){
            Logo.find({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layout){
                res.render('login', {navLinks: navLinks, layout: layout[0]});
            });
        });
    }

});

myApp.get('/page/:name',function(req, res){
    var name = req.params.name;
    console.log(name);
    Contact.find({slug: name}).exec(function(err, contacts){
        Contact.find({}).exec(function(err, navLinks){
            Logo.find({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layout){
                res.render('index', {page:contacts[0], navLinks: navLinks, layout: layout[0]});
            });

        });
    });
});
//--------------Edit-Header-------------------
myApp.get('/edit-header',function(req, res){
    if(req.session.userLoggedIn){
        Logo.findOne({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layoutdata){
            Logo.find({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layout){
                res.render('edit-header', {layoutdata:layoutdata, layout: layout[0] });
            });
        });
    }
    else {
        Contact.find({}).exec(function (err, navLinks) {
            Logo.find({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layout){
                res.render('login', {navLinks: navLinks, layout: layout[0]});
            });
        });
    }
});
myApp.post('/edit-header',function(req, res){
    //fetch all the data from URL and form
    var headertext = req.body.headertext;
    var footertext = req.body.footertext;
    //fetch the contact with the id from URL from the database
    Logo.findOne({_id:"5de5a16daf926955a4e28df6"}).exec(function(err, layout){
        //edit the fetched object from the database
        layout.headertext = headertext;
        layout.footertext = footertext;
        //save the updated contact into the database
        layout.save().then( ()=>{
            console.log('layout updated');
        });       
    });
    //redirect to all contacts page
    res.redirect('/view-pages');
});

//----------- Start the server -------------------

myApp.listen(8080);
console.log('Server started at 8080 for mywebsite...');